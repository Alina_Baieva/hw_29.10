﻿@HW_19m @moduleRegistration
Feature: Registration
	In order to play qambling 
	As a user of the application 'World of Gambling'
	I want to be able to register in this application

#Background:
#    Given Registration page is open

@p1 @smoke
Scenario Outline: Check that user can register with valid data on the Registration form
	When  I fill Registration form fields with valid data
	| Age  | Login   | Password   | Email   |
	| <age> | <login> | <password> | <email> |
	And I click the Зарегистрироватся button
	Then The user's personal account page opens
Examples:
	| age | login                | password                                           | email                     |
	| 21  | User2                | Test31                                             | t2@gm.com                 |
	| 22  | Test22               | qWert51                                            | test2@gm.com              |
	| 69  | LunaminmaxOki31995   | 75541258QweafgfGFADFHBsdzxc3pQ43564766563VDsDSGFH  | ASDF102948@mail.ru        |
	| 120 | MaximumminimumOk1995 | JGNAJLDjnJ&QweafgfGFADFHBsg4356476656?3VDsDSGFHaaa | lililmaxmax090901@post.ua |
	
#@p1 @smoke
#Scenario: Check that when clicking on the button 'Уже есть аккаунт?' user is taken to the tab Authorization form
#	When I click on the button 'Уже есть аккаунт?'
#	Then The user got to the page with Authorization form
#
#@p1 @smoke
#Scenario: Check that clicking on the button 'Контактные данные' user see popup with developer's and admin's emails
#	When I click on the button 'Контактные данные'
#	Then User see popup with developer's and admin's emails
#
#@p1
Scenario Outline: Check that user can not register with invalid data
	When Registration form fields are filled with valid data
	| Age   | Login   | Password   | Email   |
	| <age> | <login> | <password> | <email> |
	And I click the Зарегистрироватся button
	And The user remains on the Registration page
Examples:
	| age  | login                  | password                                                  | email                 |
	| 1    | User21                 | Test123                                                   | tgm1@com              |
	| test | User212                | Test1232                                                  | lilil@gmail.com       |
	| 121  | Test123                | JQwerty123                                                | llil33il@gmail.com    |
	| null | Test12                 | Qwerty123                                                 | l22lil@gmail.com      |
	| 44   | Use1                   | Test321                                                   | tgm22@com             |
	| 32   | алина                  | JQwe3rty123                                               | test2@gmail.com       |
	| 120  | cmagupwizevxritcal189+ | Qwerty123r4                                               | tgm422@com            |
	| 99   | null                   | Qwerty223r4                                               | l22li33l@gmail.com    |
	| 25   | Use1544                | Test                                                      | tgm@gmail.com         |
	| 76   | Test312                | 123987458                                                 | l2233l@gmail.com      |
	| 119  | cmagupwize             | JGNAJLDjnJ&QweafgfGFADFHBs356476656?3VDsDSGFHaaavgrRGRfff | lililmaxmax090901@com |
	| 55   | London                 | null                                                      | lilmaxmax090901@com   |
	| 47   | London23               | Test321                                                   | tgm.com               |
	| 23   | User212                | Qwerty123                                                 | авпмафв@пап.соь       |
	| 54   | cmagu55                | Te5st1232                                                 | lililmaxmax090901@    |
	| 22   | Us33e1                 | Te53st1232                                                | null                  |
#
#
#@p2
#Scenario Outline: Check that it's impossible to register in the application using login or email that already exists
#	Given There is a registered user with Login in application.
#	| Login |
#	| Test1 |
#	And There is a registered user with Email in application.
#	| Email           |
#	| test2@gm.com   |
#	When I filled the required field in the Registration form
#	| Age  | Login   | Password   | Email   |
#	| age | <login> | <password> | <email> |
#	And I click the 'Зарегистрироватся' button.
#	#Then The popup with Notification appears on the Registrartion page.
#	#| Notification   |
#	#| <notification> |
#	And The user remains on the Registration page.
#Examples:
#	| age | login   | password   | email              |
#	| 25  | Test1   | Qwerty123  | linaluna@gmail.com |
#	| 30  | Luna016 | Qwerty1234 | test2@gm.com       |

#
#@p3
#Scenario Outline: Check that  Registration form contains required fields and buttons
#	Then Make sure that the Registration form contains the required fields and buttons
#Examples:
#	| Field           | Button              |
#	| <date of birth> | <Зарегистрироватся> |
#	| <login>         | <Уже есть аккаунт?> |
#	| <password>      | <Контактные данные> |
#	| <email>         |                     |
	

