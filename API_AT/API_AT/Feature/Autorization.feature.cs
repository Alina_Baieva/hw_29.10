// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace API_AT.Feature
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Autorization")]
    [NUnit.Framework.CategoryAttribute("HW_19m")]
    [NUnit.Framework.CategoryAttribute("moduleAutorization")]
    public partial class AutorizationFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "HW_19m",
                "moduleAutorization"};
        
#line 1 "Autorization.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Autorization", "\tIn order to use the available functionality of the application\r\n\tAs a user of th" +
                    "e application \'World of Gambling\'\r\n\tI want to be able to log in this application" +
                    "", ProgrammingLanguage.CSharp, new string[] {
                        "HW_19m",
                        "moduleAutorization"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 7
 #line hidden
#line 8
 testRunner.Given("The page of authorization is open", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Authorization with valid data")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        [NUnit.Framework.TestCaseAttribute("User2", "Test2", null)]
        [NUnit.Framework.TestCaseAttribute("Lunaminmax.Oki31996", "98541258QwefgfGFADFHBsdzxc3pQ43564766563VDsDSGFH", null)]
        [NUnit.Framework.TestCaseAttribute("MaximumminimumOk1995", "TREWJLDjnJ&QweafgfGFADFHBs 4356476656?3VDsDSGFHaaa", null)]
        public virtual void AuthorizationWithValidData(string login, string password, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "p1",
                    "smoke"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Authorization with valid data", null, @__tags);
#line 11
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 12
    testRunner.Given("User is registered in the application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
                TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                            "Login",
                            "Password"});
                table1.AddRow(new string[] {
                            string.Format("{0}", login),
                            string.Format("{0}", password)});
#line 13
 testRunner.When("I fill Autorization form fields with valid data", ((string)(null)), table1, "When ");
#line hidden
#line 16
 testRunner.And("I click on \'Войти\' button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 17
 testRunner.Then("The user\'s personal account page is open", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check that an unregistered user cannot log in")]
        [NUnit.Framework.CategoryAttribute("p1")]
        public virtual void CheckThatAnUnregisteredUserCannotLogIn()
        {
            string[] tagsOfScenario = new string[] {
                    "p1"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check that an unregistered user cannot log in", null, new string[] {
                        "p1"});
#line 25
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 26
 testRunner.Given("User is not registered in the application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
                TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                            "Login",
                            "Password"});
                table2.AddRow(new string[] {
                            "Mily",
                            "Qwerty123"});
#line 27
 testRunner.When("I fill authorization field", ((string)(null)), table2, "When ");
#line hidden
#line 30
 testRunner.And("I click on \'Войти\' button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 31
 testRunner.Then("User see popup \'IНеверный логин и/или пароль.... Попробуйте снова\'  on the displa" +
                        "y", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
#line 32
 testRunner.And("User stay on Autorization page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check that when clicking on the button \'Зарегистрироваться\' user is taken to tab " +
            "Registration form")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        public virtual void CheckThatWhenClickingOnTheButtonЗарегистрироватьсяUserIsTakenToTabRegistrationForm()
        {
            string[] tagsOfScenario = new string[] {
                    "p1",
                    "smoke"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check that when clicking on the button \'Зарегистрироваться\' user is taken to tab " +
                    "Registration form", null, new string[] {
                        "p1",
                        "smoke"});
#line 35
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 36
 testRunner.When("I click on the button \'Зарегистрироваться\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 37
 testRunner.Then("The user got to the page with Registration form", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check that when clicking on the button \'Контактные данные\' user see popup with de" +
            "veloper\'s and admin\'s emails")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        public virtual void CheckThatWhenClickingOnTheButtonКонтактныеДанныеUserSeePopupWithDevelopersAndAdminsEmails()
        {
            string[] tagsOfScenario = new string[] {
                    "p1",
                    "smoke"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check that when clicking on the button \'Контактные данные\' user see popup with de" +
                    "veloper\'s and admin\'s emails", null, new string[] {
                        "p1",
                        "smoke"});
#line 40
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 41
 testRunner.When("I click on the button \'Контактные данные\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 42
 testRunner.Then("User see popup with developer\'s and admin\'s emails", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check that after clicking on the button \'Забыли пароль?\', user see popup  with a " +
            "password recovery form")]
        [NUnit.Framework.CategoryAttribute("p1")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        public virtual void CheckThatAfterClickingOnTheButtonЗабылиПарольUserSeePopupWithAPasswordRecoveryForm()
        {
            string[] tagsOfScenario = new string[] {
                    "p1",
                    "smoke"};
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check that after clicking on the button \'Забыли пароль?\', user see popup  with a " +
                    "password recovery form", null, new string[] {
                        "p1",
                        "smoke"});
#line 45
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 46
 testRunner.When("I click on the button \'Забыли пароль?\'", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 47
 testRunner.Then("User see popup form for recovery user\'s password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check that the Autorization form contains required fields and buttons")]
        [NUnit.Framework.CategoryAttribute("p3")]
        [NUnit.Framework.TestCaseAttribute("<login>", "<Уже есть аккаунт?>", null)]
        [NUnit.Framework.TestCaseAttribute("<password>", "<Забыли пароль?>", null)]
        [NUnit.Framework.TestCaseAttribute("", "<Войти>", null)]
        [NUnit.Framework.TestCaseAttribute("", "<Зарегистрироватся>", null)]
        public virtual void CheckThatTheAutorizationFormContainsRequiredFieldsAndButtons(string field, string button, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "p3"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check that the Autorization form contains required fields and buttons", null, @__tags);
#line 50
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 7
 this.FeatureBackground();
#line hidden
#line 51
 testRunner.Then("Make sure that the Autorization contains the required fields and buttons", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
