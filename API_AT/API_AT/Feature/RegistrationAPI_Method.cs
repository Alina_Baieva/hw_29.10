﻿using API_AT.Support;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_AT.Feature
{
   public class RegistrationAPI_Method
    {
        private static RestClient _client;
        private static RestRequest _request;
        public static IRestResponse _response { get; set; }
        internal static void ConfigureRequest(string url)
        {
            _client = new RestClient(url)
            {
                Timeout = 60000
            };
        }
        internal static void FillRegistrationFields(int age, string login, string password, string email )
        {
            _request = new RestRequest(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"age", age},
                {"email", email },
                {"login", login },
                {"password", password}
            };
           
            _request.AddJsonBody(parameters);

        }
        internal static void SendRequest()
        {
            var response = _client.Execute(_request);
            _response = response;
        }
    }
}
