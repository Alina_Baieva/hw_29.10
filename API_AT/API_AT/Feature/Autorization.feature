﻿@HW_19m @moduleAutorization
Feature: Autorization
	In order to use the available functionality of the application
	As a user of the application 'World of Gambling'
	I want to be able to log in this application

	Background: 
	Given The page of authorization is open
	
@p1 @smoke
Scenario Outline: Authorization with valid data
    Given User is registered in the application
	When I fill Autorization form fields with valid data
		| Login   | Password   |
		| <login> | <password> |
	And I click on 'Войти' button
	Then The user's personal account page is open
Examples:
		| login                | password                                           |
		| User2                | Test2                                              |
		| Lunaminmax.Oki31996  | 98541258QwefgfGFADFHBsdzxc3pQ43564766563VDsDSGFH   |
		| MaximumminimumOk1995 | TREWJLDjnJ&QweafgfGFADFHBs 4356476656?3VDsDSGFHaaa |

@p1 
Scenario: Check that an unregistered user cannot log in
	Given User is not registered in the application
	When I fill authorization field
		| Login | Password  |
		| Mily  | Qwerty123 |
	And I click on 'Войти' button
	Then User see popup 'IНеверный логин и/или пароль.... Попробуйте снова'  on the display
	And User stay on Autorization page

@p1 @smoke
Scenario: Check that when clicking on the button 'Зарегистрироваться' user is taken to tab Registration form
	When I click on the button 'Зарегистрироваться'
	Then The user got to the page with Registration form

@p1 @smoke
Scenario: Check that when clicking on the button 'Контактные данные' user see popup with developer's and admin's emails
	When I click on the button 'Контактные данные'
	Then User see popup with developer's and admin's emails

@p1 @smoke
Scenario: Check that after clicking on the button 'Забыли пароль?', user see popup  with a password recovery form
	When I click on the button 'Забыли пароль?'
	Then User see popup form for recovery user's password

@p3
Scenario Outline: Check that the Autorization form contains required fields and buttons
	Then Make sure that the Autorization contains the required fields and buttons
Examples:
	| Field      | Button              |
	| <login>    | <Уже есть аккаунт?> |
	| <password> | <Забыли пароль?>    |
	|            | <Войти>             |
	|            | <Зарегистрироватся> |
		