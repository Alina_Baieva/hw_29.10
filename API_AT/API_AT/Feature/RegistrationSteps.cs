﻿using NUnit.Framework;
using System;
using System.Net;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace API_AT.Feature
{
    [Binding]
    public class RegistrationSteps
    {


        [When(@"I fill Registration form fields with valid data")]
        public void WhenIFillRegistrationFormFieldsWithValidData(Table table)
        {
            RegistrationAPI_Method.ConfigureRequest("http://localhost:4221/registration");

            dynamic data = table.CreateDynamicInstance();
            RegistrationAPI_Method.FillRegistrationFields((int)data.Age, (string)data.Login, (string)data.Password, (string)data.Email);

        }

        [When(@"I click the Зарегистрироватся button")]
        public void WhenIClickTheЗарегистрироватсяButton()
        {
            RegistrationAPI_Method.SendRequest();
        }


        [Then(@"The user's personal account page opens")]
        public void ThenTheUserSPersonalAccountPageOpens()
        {
            var actualStatusCode = RegistrationAPI_Method._response.StatusCode;
            Assert.AreEqual(HttpStatusCode.Created, actualStatusCode);
        }
        [When(@"Registration form fields are filled with valid data")]
        public void WhenRegistrationFormFieldsAreFilledWithValidData(Table table)
        {
            RegistrationAPI_Method.ConfigureRequest("http://localhost:4221/registration");

            dynamic data = table.CreateDynamicInstance();
            RegistrationAPI_Method.FillRegistrationFields((int)data.Age, (string)data.Login, (string)data.Password, (string)data.Email);
        }

        [When(@"The user remains on the Registration page")]
        public void WhenTheUserRemainsOnTheRegistrationPage()
        {
            var actualStatusCode = RegistrationAPI_Method._response.StatusCode;
            Assert.AreEqual(HttpStatusCode.BadRequest, actualStatusCode);
        }
        

    }
}
