﻿using API_AT.API.Models;
using API_AT.Support;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace API_AT.API.Request
{
    public class Auth
    {
        internal static HttpStatusCode AuthPost(string login, string password)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/authorization");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"login", login},
                {"password", password}
            };
            ApiRequest.SetData(parameters);

            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
        internal static string AuthToken(string login, string password)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/authorization");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"login", login},
                {"password", password}
            };
            ApiRequest.SetData(parameters);
            var response = ApiRequest.SendRequest();

            return response.Content;
            //return JsonConvert.DeserializeObject<AuthModel>(response.Content);
        }
    }
}
