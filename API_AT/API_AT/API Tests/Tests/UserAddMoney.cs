﻿using API_AT.API.Request;
using NUnit.Framework;
using System.Net;

namespace API_AT.Tests
{
    class UserAddMoney
    {
        [TestCase(400, "Cevoni0378", "n49bpxeCYGALb")]//klient admin 1 
        [TestCase(800, "nAstya531013185", "n49bpxeCYGALb")]//klient admin 2 
        public void AddMoney(int sum, string login, string password)
        {
            var token = Auth.AuthToken(login, password);
            var responseStatusCode = SendMoney.TopUpWallet(sum, token);

            Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);
        }
    }
}
