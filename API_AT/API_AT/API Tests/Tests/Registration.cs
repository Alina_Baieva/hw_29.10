﻿using API_AT.API.Request;
using NUnit.Framework;
using System.Net;

namespace API_AT
{
    class Registration
    {
        public string NewMail() => TempEmail.GenerateMail();
        public string NewLogin() => Login.MakeLogin();

        //[TestCase(24, "n49bpxeCYGALb")]
        [TestCase(50, "n4938279iu2qLb")]
        public void ValidRegistration(int age, string password)
        {
            var statusCode = Registr.RegistrationPost(age, NewMail(), NewLogin(), password);  
            
            Assert.AreEqual(HttpStatusCode.Created, statusCode);
        }

        [TestCase(24, "cevoni6847@settags.com", "Cevoni0378", "n49bpxeCYGALb")]
        public void InvalidRegistration(int age, string email, string login, string password)
        {
            var statusCode = Registr.RegistrationPost(age, email, login, password);

            Assert.AreEqual(HttpStatusCode.BadRequest, statusCode);
        }
    }
}
