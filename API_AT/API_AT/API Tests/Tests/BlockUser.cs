﻿using API_AT.API.Request;
using NUnit.Framework;
using System.Net;

namespace API_AT
{
    /// <summary>
    /// The controller is responsible for block/unblock user.
    /// </summary>
    class BlockUser
    {
        [TestCase(false, "nAstya531013185")]//block
        [TestCase(true, "nAstya531013185")]//unblock
        public void BlockUSER(bool wasBlock, string login)
        {           
            var actualTocken = Auth.AuthToken("Admin", "000GGGggg000");
            var responseStatusCode = Block.BlockPost(wasBlock, login, actualTocken);

            Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);
        }
    }
}
